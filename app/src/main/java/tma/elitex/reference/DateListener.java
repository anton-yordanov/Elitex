package tma.elitex.reference;

/**
 * Created by Krum Iliev.
 */
public interface DateListener {
    void onDateSet (int year, int month, int day);
}
